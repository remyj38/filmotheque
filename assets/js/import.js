$('#file').change(function(e){ // Lors du chargement du fichier, on récupère tous les elements
    $('#content').empty();
    files = $("#file")[0].files;
    var reader  = new FileReader();
    reader.onload = function(event) {
        var content = $.parseJSON(event.target.result);
        console.log(content);
        $.each(content, function(key, value){
            console.log(value);
            $('#content').append("<li>"+value.Name+"</li>");
        });
    }
    reader.readAsText(files[0]);
    console.log(reader.result)
});