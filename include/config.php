<?php

/* Définition des variables globales de la base de données */
define('DB_USER', 'root');
define('DB_PASSWD', '');
define('DB_NAME', 'filmotheque');
define('DB_HOST', 'localhost');
define('DB_PORT', 3306);
define('FORMAT_DATE', 'd F Y');
define('BASE_URL', 'http://localhost/films/');
