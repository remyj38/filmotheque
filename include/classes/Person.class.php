<?php

/**
 * Description of Person
 *
 * @author Rémy
 */
class Person {

    static public $persons;
    protected $idPerson;
    protected $firstName;
    protected $lastName;
    /* @var Nationality */
    protected $nationality;
    protected $gender;
    protected $link;
    protected $role = null;

    public function __construct(array $args = array()) {
        foreach ($args as $key => $value) {
            if ($key != 'nationality') {
                $this->$key = $value;
            } else {
                $this->nationality = Nationality::get($value);
            }
        }
    }

    /**
     * Add a person
     * @global PDO $db
     * @param string $name
     * @throws Exception
     */
    static public function add(array $args) {
        global $db;
        reset($args);
        $firstKey = key($args);
        $sql = 'INSERT INTO persons VALUES (';
        foreach ($args as $key => $value) {
            if ($firstKey == $key) {
                $sql .= ':' . $key;
            } else {
                $sql .= ', :' . $key;
            }
        }
        $sql .= ')';
        $request = $db->prepare($sql);
        foreach ($args as $key => $value)
            $request->bindValue($key, $value);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        self::$persons[$args['idPerson']] = new Person($args);
    }

    public function getId() {
        return $this->idPerson;
    }

    public function getFirstname() {
        return $this->firstName;
    }

    public function getLastname() {
        return $this->lastName;
    }

    public function getName() {
        return $this->firstName . ' ' . $this->lastName;
    }

    /* @return Nationality */

    public function getNationality() {
        return $this->nationality;
    }

    public function getGender($asString = false) {
        if ($asString)
            return ($this->gender === 1) ? 'homme' : 'femme';
        else
            return $this->gender;
    }

    public function getLink($asHtml = false) {
        if ($asHtml)
            return '<a href="' . $this->link . '" target="_blank" title="' . $this->firstName . ' ' . $this->lastName . '">Voir la fiche Allociné</a>';
        else
            return $this->link;
    }

    public function getPosterURL() {
        if (file_exists('medias/photos/' . $this->idPerson . '.png'))
            return BASE_URL . 'medias/photos/' . $this->idPerson . '.png';
        else
            return BASE_URL . 'medias/photos/default.png';
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role) {
        $this->role = $role;
    }

    /**
     * Get an Person
     * @global PDO $db
     * @param int $id
     * @return Person|false
     * @throws Exception
     */
    static public function get($id, $role = null) {
        if (!self::$persons) {
            try {
                self::getAll();
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        if (isset(self::$persons[$id])) {
            $return = self::$persons[$id];
            if ($role)
                $return->setRole($role);
            return $return;
        } else
            return false;
    }

    /**
     * 
     * @global PDO $db
     * @return Person[]
     * @throws Exception
     */
    static public function getAll() {
        if (!self::$persons) {
            global $db;
            $request = $db->prepare('SELECT * FROM persons');
            if (!$request->execute())
                throw new Exception('Echec lors de l\'execution d\'une requete');
            while ($person = $request->fetch(PDO::FETCH_ASSOC)) {
                self::$persons[$person['idPerson']] = new Person($person);
            }
        }
        return self::$persons;
    }

    /**
     * 
     * @global PDO $db
     * @param int $idMovie
     * @return Person[]
     * @throws Exception
     */
    static public function getByMovie($idMovie) {
        global $db;
        $request = $db->prepare('SELECT actor, activity, role FROM movies_has_persons WHERE movie = :idMovie ORDER BY importance ASC');
        $request->bindValue('idMovie', $idMovie);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        while ($datas = $request->fetch(PDO::FETCH_ASSOC)) {
            $return[$datas['activity']][] = Person::get($datas['actor'], $datas['role']);
        }
        return $return;
    }

}
