<?php

/**
 * Description of Role
 *
 * @author Rémy
 */
class Role {

    static public $roles;
    protected $idRole;
    protected $name;

    public function __construct(array $args = array()) {
        foreach ($args as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Add a role
     * @global PDO $db
     * @param int $id
     * @param string $name
     * @throws Exception
     */
    static public function add($id, $name) {
        global $db;
        $request = $db->prepare('INSERT INTO roles VALUES (:id, :name)');
        $request->bindValue('id', $id);
        $request->bindValue('name', $name);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        self::$roles[$id] = new Role(array('idRole' => $id, 'name' => $name));
    }

    public function getId() {
        return $this->idRole;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @global PDO $db
     * @param string $name
     * @return boolean
     */
    public function setName($name) {
        $this->name = $name;
        global $db;
        $request = $db->prepare('UPDATE roles SET name = :name WHERE `idRole` = :id');
        $request->bindValue('name', $name);
        $request->bindValue('id', $this->idRole, PDO::PARAM_INT);
        return $request->execute();
    }

    /**
     * Get an Role
     * @global PDO $db
     * @param int $id
     * @return Role|false
     * @throws Exception
     */
    static public function get($id) {
        if (!self::$roles) {
            try {
                self::getAll();
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        return isset(self::$roles[$id]) ? self::$roles[$id] : false;
    }

    /**
     * 
     * @global PDO $db
     * @return Role[]
     * @throws Exception
     */
    static public function getAll() {
        if (!self::$roles) {
            global $db;
            $request = $db->prepare('SELECT * FROM roles');
            if (!$request->execute())
                throw new Exception('Echec lors de l\'execution d\'une requete');
            foreach ($request->fetchAll(PDO::FETCH_CLASS, 'Role') as $role)
                self::$roles[$role->getId()] = $role;
        }
        return self::$roles;
    }

}
