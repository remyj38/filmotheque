<?php

/**
 *
 * @author Rémy
 */
class Movie {

    protected $idMovie;
    protected $originalTitle;
    protected $title;
    protected $productionYear;
    protected $runtime;
    protected $synopsis;
    protected $link;
    protected $trailerId;
    protected $pressRating;
    protected $userRating;
    protected $file;
    /* @var Genre */
    protected $genres = array();
    /* @var Nationality */
    protected $nationalities = array();
    /* @var array */
    protected $roles = array();

    public function __construct(array $args = array()) {
        foreach ($args as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Add a movie
     * @global PDO $db
     * @param array $args
     * @throws Exception
     */
    static public function add($args) {
        global $db;
        reset($args);
        $firstKey = key($args);
        $sql = 'INSERT INTO movies VALUES (';
        foreach ($args as $key => $value) {
            if ($firstKey == $key) {
                $sql .= ':' . $key;
            } else {
                $sql .= ', :' . $key;
            }
        }
        $sql .= ')';
        $request = $db->prepare($sql);
        foreach ($args as $key => $value)
            $request->bindValue($key, $value);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
    }

    /**
     * Ajoute la liaison entre un film et un genre
     * @global PDO $db
     * @param int $movie
     * @param int $genre
     * @throws Exception
     */
    static public function addGenre($movie, $genre) {
        global $db;
        $request = $db->prepare('INSERT INTO movies_has_genres VALUES (:movie, :genre)');
        $request->bindValue('movie', $movie);
        $request->bindValue('genre', $genre);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
    }

    /**
     * Ajoute la liaison entre un film et une nationnalité
     * @global PDO $db
     * @param int $movie
     * @param int $nationality
     * @throws Exception
     */
    static public function addNationality($movie, $nationality) {
        global $db;
        $request = $db->prepare('INSERT INTO movies_has_nationalities VALUES (:movie, :nationality)');
        $request->bindValue('movie', $movie);
        $request->bindValue('nationality', $nationality);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
    }

    /**
     * Ajoute la liaison entre un film et une personne
     * @global PDO $db
     * @param int $movie
     * @param int $nationality
     * @throws Exception
     */
    static public function addPerson($movie, $person, $activity, $importance, $role = null) {
        global $db;
        $request = $db->prepare('INSERT INTO movies_has_persons VALUES (:movie, :person, :activity, :role, :importance)');
        $request->bindValue('movie', $movie);
        $request->bindValue('person', $person);
        $request->bindValue('activity', $activity);
        $request->bindValue('role', $role);
        $request->bindValue('importance', $importance);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
    }

    public function getIdMovie() {
        return $this->idMovie;
    }

    public function getOriginalTitle() {
        return $this->originalTitle;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getProductionYear() {
        return $this->productionYear;
    }

    public function getRuntime() {
        return $this->runtime;
    }

    public function getSynopsis($html = false) {
        if ($html) {
            return str_replace("\n", "<br />\n", $this->synopsis);
        }
        return $this->synopsis;
    }

    public function getLink() {
        return $this->link;
    }

    public function getIframeTrailer() {
        return '<iframe src=\'http://www.allocine.fr/_video/iblogvision.aspx?cmedia=' . $this->trailerId . '\' style=\'width:960px; height:540px\' frameborder=\'0\' allowfullscreen=\'true\' class=".embed-responsive-item">Vous devez activer les iframes pour voir cette vidéo</iframe>';
    }

    public function getPressRating() {
        return $this->pressRating;
    }

    public function getUserRating() {
        return $this->userRating;
    }

    public function getFile() {
        return $this->file;
    }

    public function getGenres() {
        return $this->genres;
    }

    public function getNationalities() {
        return $this->nationalities;
    }

    public function getRoles() {
        return $this->roles;
    }

    /**
     * Get a movie
     * @global PDO $db
     * @param int $id
     * @return Role|false
     * @throws Exception
     */
    static public function get($id) {
        global $db;
        $request = $db->prepare('SELECT * FROM movies WHERE `idMovie` = :id');
        $request->bindValue('id', $id);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        if (!$request->rowCount())
            throw new Exception('Film introuvable');
        $datas = $request->fetch(PDO::FETCH_ASSOC);
        $datas['genres'] = Genre::getByMovie($id);
        $datas['nationalities'] = Nationality::getByMovie($id);
        $datas['roles'] = Person::getByMovie($id);
        return new Movie($datas);
    }

    /**
     * 
     * @global PDO $db
     * @return Movie[]
     * @throws Exception
     */
    static public function getAll() {
        global $db;
        $request = $db->prepare('SELECT `idMovie` FROM movies ORDER BY title');
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        while ($movie = $request->fetch(PDO::FETCH_ASSOC)) {
            try {
                $return[$movie['idMovie']] = Movie::get($movie['idMovie']);
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        return $return;
    }

}
