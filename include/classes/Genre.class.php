<?php

/**
 * Description of Genre
 *
 * @author Rémy
 */
class Genre {

    static public $genres;
    protected $idGenre;
    protected $name;

    public function __construct(array $args = array()) {
        foreach ($args as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Add a genre
     * @global PDO $db
     * @param int $id
     * @param string $name
     * @throws Exception
     */
    public static function add($id, $name) {
        global $db;
        $request = $db->prepare('INSERT INTO genres VALUE (:idGenre, :name)');
        $request->bindValue('idGenre', $id);
        $request->bindValue('name', $name);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        self::$genres[$id] = new Genre(array('idGenre' => $id, 'name' => $name));
    }

    public function getId() {
        return $this->idGenre;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @global PDO $db
     * @param string $name
     * @return boolean
     */
    public function setName($name) {
        $this->name = $name;
        global $db;
        $request = $db->prepare('UPDATE genres SET name = :name WHERE `idGenre` = :id');
        $request->bindValue('name', $name);
        $request->bindValue('id', $this->idGenre, PDO::PARAM_INT);
        return $request->execute();
    }

    /**
     * Get an object
     * @global PDO $db
     * @param int $id
     * @return Genre|false
     * @throws Exception
     */
    static public function get($id) {
        if (!self::$genres) {
            try {
                self::getAll();
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        return isset(self::$genres[$id]) ? self::$genres[$id] : false;
    }

    /**
     * 
     * @global PDO $db
     * @return Genre[]
     * @throws Exception
     */
    static public function getAll() {
        if (!self::$genres) {
            global $db;
            $request = $db->prepare('SELECT * FROM genres');
            if (!$request->execute())
                throw new Exception('Echec lors de l\'execution d\'une requete');
            foreach ($request->fetchAll(PDO::FETCH_CLASS, 'Genre') as $genre)
                self::$genres[$genre->getId()] = $genre;
        }
        return self::$genres;
    }

    /**
     * 
     * @global PDO $db
     * @param int $idMovie
     * @return Genre[]
     * @throws Exception
     */
    static public function getByMovie($idMovie) {
        global $db;
        $request = $db->prepare('SELECT genre FROM movies_has_genres WHERE movie = :idMovie');
        $request->bindValue('idMovie', $idMovie);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete ');
        while ($genre = $request->fetchColumn()) {
            $genre = Genre::get($genre);
            $return[$genre->getId()] = $genre;
        }
        return $return;
    }

}
