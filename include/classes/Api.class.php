<?php

/**
 * @author Rémy
 */
class Api {

    private $params;
    private $return = array('code' => '200', 'messages' => array());
    private $allocine;

    public function __construct($params) {
        if (isset($params))
            $this->params = $params;
        $this->allocine = new Allocine();
    }

    /**
     * Execute la requete
     * @throws ApiError
     */
    public function exec() {
        if (!isset($this->params['method']))
            throw new ApiError("Missing method", 400);
        if ($this->params['method'] == 'addFilm') {
            if (!isset($this->params['file']))
                throw new ApiError("Missing arguments", 400);
            $name = preg_replace('#^(.*) \(([\d]{4})\)\.[\w]{1,4}$#', '$1', $this->params['file']);
            $year = preg_replace('#^(.*) \(([\d]{4})\)\.[\w]{1,4}$#', '$2', $this->params['file']);
            try {
                $this->addFilm($name, $year, $this->params['file']);
            } catch (ApiError $ex) {
                throw $ex;
            }
        }
    }

    private function addFilm($name, $year, $file) {
        if (!($id = $this->allocine->searchMovie($name, (int) $year)))
            throw new ApiError('Unknow movie', 404);
        $movie = $this->allocine->getMovie($id);
        try { // Test si le film est déjà existant
            Movie::get($id);
            $exist = true;
        } catch (Exception $ex) {
            $exist = false;
        }
        if ($exist)
            throw new ApiError('Existing movie', 200);

        /* Insertion du film */
        $args = array(
            'idMovie' => $id,
            'originalTitle' => $movie['originalTitle'],
            'title' => $movie['title'],
            'productionYear' => $movie['productionYear'],
            'runtime' => $movie['runtime'],
            'synopsis' => $movie['synopsis'],
            'link' => $movie['link'][0]['href'],
            'trailerId' => $movie['trailer']['code'],
            'pressRating' => $movie['statistics']['pressRating'],
            'userRating' => $movie['statistics']['userRating'],
            'file' => $file,
        );
        try {
            Movie::add($args);
        } catch (Exception $ex) {
            throw new ApiError($ex->getMessage(), 503);
        }
        /* Insertion des genres */
        foreach ($movie['genre'] as $genre) {
            if (!Genre::get($genre['code'])) {
                try {
                    Genre::add($genre['code'], $genre['$']);
                } catch (Exception $ex) {
                    $this->return['code'] = 500;
                    $this->return['messages'][] = 'Echec lors de l\'ajout d\'un genre : ' . $genre['$'];
                }
            }
            try {
                Movie::addGenre($id, $genre['code']);
            } catch (Exception $ex) {
                $this->return['code'] = 500;
                $this->return['messages'][] = 'Echec lors de l\'ajout de la liaison entre le film et le genre : ' . $genre['$'];
            }
        }

        /* Insertion des nationnalités */
        foreach ($movie['nationality'] as $nationality) {
            if (!Nationality::get($nationality['code'])) {
                try {
                    Nationality::add($nationality['code'], $nationality['$']);
                } catch (Exception $ex) {
                    $this->return['code'] = 500;
                    $this->return['messages'][] = 'Echec lors de l\'ajout d\'une nationnalité : ' . $nationality['$'];
                }
            }
            try {
                Movie::addNationality($id, $nationality['code']);
            } catch (Exception $ex) {
                $this->return['code'] = 500;
                $this->return['messages'][] = 'Echec lors de l\'ajout de la liaison entre le film et la nationnalité : ' . $nationality['$'];
            }
        }
        /* Insertion des personnes */
        $images = array();
        $i = 0; //Compteur pour ordre des personnes
        foreach ($movie['castMember'] as $person) {
            if (!Person::get($person['person']['code'])) { // Ajout de la personne
                try {
                    $personSource = $this->allocine->getPerson($person['person']['code']);
                    if (isset($personSource['nationality'])) {
                        if (!Nationality::get($personSource['nationality'][0]['code'])) { // Ajout de la nationnalité de la personne si besoin
                            try {
                                Nationality::add($personSource['nationality'][0]['code'], $personSource['nationality'][0]['$']);
                            } catch (Exception $ex) {
                                $this->return['code'] = 500;
                                $this->return['messages'][] = 'Echec lors de l\'ajout d\'une nationnalité : ' . $personSource['nationality'][0]['$'];
                            }
                        }
                    }
                    Person::add(array(
                        'idPerson' => $personSource['code'],
                        'firstName' => (isset($personSource['name']['given'])) ? $personSource['name']['given'] : null,
                        'lastName' => $personSource['name']['family'],
                        'nationality' => (isset($personSource['nationality'])) ? $personSource['nationality'][0]['code'] : 0,
                        'gender' => (isset($personSource['gender'])) ? $personSource['gender'] : null,
                        'link' => $personSource['link'][0]['href'],
                    ));
                    if (isset($personSource['picture']['href']))
                        $images[$personSource['code']] = $personSource['picture']['href'];
                } catch (Exception $ex) {
                    $this->return['code'] = 500;
                    $this->return['messages'][] = 'Echec lors de l\'ajout d\'une personne : ' . $person['person']['name'];
                }
            }
            if (!Role::get($person['activity']['code'])) { // Ajout du role si nécessaire
                try {
                    Role::add($person['activity']['code'], $person['activity']['$']);
                } catch (Exception $ex) {
                    $this->return['code'] = 500;
                    $this->return['messages'][] = 'Echec lors de l\'ajout d\'un role : ' . $person['activity']['$'];
                }
            }
            try { // Ajout du lien film / personne / role
                if (isset($person['role']))
                    Movie::addPerson($id, $person['person']['code'], $person['activity']['code'], $i, $person['role']);
                else
                    Movie::addPerson($id, $person['person']['code'], $person['activity']['code'], $i);
            } catch (Exception $ex) {
                $this->return['code'] = 500;
                $this->return['messages'][] = 'Echec lors de l\'ajout de la liaison entre le film et la personne : ' . $person['person']['name'];
            }
            $i++;
        }

        /* Récupération des images */
        if (isset($movie['poster']['href']))
            if (download_image($movie['poster']['href'], 'medias/posters/', $id))
                resizeImage('medias/posters/' . $id . '.png', 'medias/posters/mini/' . $id . '.png');
        foreach ($images as $key => $path){
            resizeImage($path, 'medias/photos/' . $key . '.png');
        }
    }

    public function result($toJSON) {
        echo ($toJSON) ? json_encode($this->return) : $return;
    }

}

class ApiError extends Exception {

    public function throwFunction() {
        header("HTTP/1.0 $this->code");
        echo json_encode(
                array(
                    "message" => $this->message,
                    "code" => $this->code,
        ));
        exit();
    }

}
