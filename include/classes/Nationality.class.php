<?php

/**
 * Description of Nationality
 *
 * @author Rémy
 */
class Nationality {

    static public $nationalities;
    protected $idNationality;
    protected $name;

    public function __construct(array $args = array()) {
        foreach ($args as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Add a nationality
     * @global PDO $db
     * @param int $id
     * @param string $name
     * @throws Exception
     */
    static public function add($id, $name) {
        global $db;
        $request = $db->prepare('INSERT INTO nationalities VALUES (:id, :name)');
        $request->bindValue('id', $id);
        $request->bindValue('name', $name);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        self::$nationalities[$id] = new Nationality(array('idNationality' => $id, 'name' => $name));    }

    public function getId() {
        return $this->idNationality;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @global PDO $db
     * @param string $name
     * @return boolean
     */
    public function setName($name) {
        $this->name = $name;
        global $db;
        $request = $db->prepare('UPDATE nationalities SET name = :name WHERE `idNationality` = :id');
        $request->bindValue('name', $name);
        $request->bindValue('id', $this->idNationality, PDO::PARAM_INT);
        return $request->execute();
    }

    /**
     * Get an nationality
     * @global PDO $db
     * @param int $id
     * @return Nationality|false
     * @throws Exception
     */
    static public function get($id) {
        if (!self::$nationalities) {
            try {
                self::getAll();
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        return isset(self::$nationalities[$id]) ? self::$nationalities[$id] : false;
    }

    /**
     * 
     * @global PDO $db
     * @return Nationality[]
     * @throws Exception
     */
    static public function getAll() {
        if (!self::$nationalities) {
            global $db;
            $request = $db->prepare('SELECT * FROM nationalities');
            if (!$request->execute())
                throw new Exception('Echec lors de l\'execution d\'une requete');
            foreach ($request->fetchAll(PDO::FETCH_CLASS, 'Nationality') as $nationality)
                self::$nationalities[$nationality->getId()] = $nationality;
        }
        return self::$nationalities;
    }

    /**
     * 
     * @global PDO $db
     * @param int $idMovie
     * @return Nationality[]
     * @throws Exception
     */
    static public function getByMovie($idMovie) {
        global $db;
        $request = $db->prepare('SELECT nationality FROM movies_has_nationalities WHERE movie = :idMovie');
        $request->bindValue('idMovie', $idMovie);
        if (!$request->execute())
            throw new Exception('Echec lors de l\'execution d\'une requete');
        while ($nationality = $request->fetchColumn()) {
            $nationality = Nationality::get($nationality);
            $return[$nationality->getId()] = $nationality;
        }
        return $return;
    }

}
