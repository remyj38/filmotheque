<?php

$months = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

function load() {
    require_once 'include/config.php';
    require_once 'include/classes/Genre.class.php';
    require_once 'include/classes/Movie.class.php';
    require_once 'include/classes/Nationality.class.php';
    require_once 'include/classes/Person.class.php';
    require_once 'include/classes/Role.class.php';
    require_once 'lib/allocine.class.php';
}

/**
 * Permet de récupèrer la connexion à la base de données
 * 
 * @return \PDO|boolean
 */
function getDbConnection() {
    try { // Essai de connexion à la BDD
        $bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';port=' . DB_PORT, DB_USER, DB_PASSWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        return $bdd; // Si connexion réussie, retour de l'objet PDO
    } catch (PDOException $e) {
        return false;
    }
}

/**
 * afficherVue - Permet d'afficher la vue demandée
 * 
 * @global array $alerts Tableau des alertes
 * @global varchar $title Titre de la page
 * @global array $mounths
 * @global pdo $db
 * @param string $view Nom de la vue
 * @param array $datas Optionnal Données à transmettre à la vue
 */
function showView($view, $datas = NULL) {
    global $alerts, $title, $months, $db;
    $path = 'include/views/' . $view . '.php';
    if (file_exists($path)) {
        include 'include/header.php';
        include $path;
        include 'include/footer.php';
    } else {
        errors(404);
    }
    exit();
}

/**
 * Permet d'initialiser le necessaire
 * @global PDO $db
 * @global array $alerts
 */
function init() {
    global $db;
    global $alerts;
    load();
    /* Récupération et test de la connexion à la base de données */
    $db = getDbConnection();
    if (!$db) { // Si la connexion a échouée, on affiche l'erreur
        errors(1001);
    }
    if (isset($_SESSION['alerts'])) {
        $alerts = $_SESSION['alerts'];
        unset($_SESSION['alerts']);
    } else {
        $alerts = array();
    }
}

/**
 * Permet de convertir un timestamp en date francaise suivant un format défini
 * @global array $months
 * @param string $format Format (CF doc date())
 * @param int $date timestamp
 * @return string
 */
function dateFr($format, $date = 'time()') {
    global $months;
    $formated = date($format, $date);
    $monthsEn = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    return strtr($formated, array_combine($monthsEn, $months));
}

/**
 * errors - Permet de générer les erreurs
 * 
 * @global array $alerts Tableau des alertes
 * @global varchar $title Titre de la page
 * @global PDO $db
 * @param int $id Id HTTP de l'erreur ou si id > 1000, erreurs de script
 * @param string $text Optionnal Texte d'erreur si erreur inconnue
 * @param string $type Optionnal type de l'erreur si erreur inconnue
 */
function errors($id, $text = NULL, $type = NULL) {
    global $alerts, $title, $db, $server;
    switch ($id) { // On affiche l'erreur suivant son id
        case 401 :
            header("HTTP/1.0 401 Unauthorized");
            $title = "Accès interdit !";
            $alerts[] = array(
                "text" => "Vous n'avez pas les autorisations nécessaire pour faire ça",
                "type" => "error"
            );
            break;
        case 403 :
            header("HTTP/1.0 403 Forbidden");
            $title = "Accès interdit !";
            $alerts[] = array(
                "text" => "Vous n'avez pas les autorisations nécessaire pour accéder à cette page",
                "type" => "error"
            );
            break;
        case 404 :
            header("HTTP/1.0 404 Not Found");
            $title = "Page introuvable !";
            $alerts[] = array(
                "text" => "Cette page n'a pas été trouvée.<br />
    Elle a surement été déplacée ou supprimée !",
                "type" => "error"
            );
            break;
        case 1001: // Base de données injoignable
            header("HTTP/1.0 503 Service Unavailable");
            $title = "Base de données injoignable";
            $alerts[] = array(
                "text" => "Désolé, la base de données est actuellement injoignable.",
                "type" => "error"
            );
            break;
        default:
            $title = $text;
            $alerts[] = array(
                "text" => $text,
                "type" => $type
            );
            break;
    }
    include 'include/header.php';
    include 'include/footer.php';
    exit();
}

/**
 * protectGet - Permet de proteger les arguments de l'url contre la faille include
 * @param type $argument argument à verifier
 * @return boolean Si l'argument est correct ou non
 */
function protectGet($argument) {
    if (preg_match("#^https?://#", $argument) || preg_match("#\.?\./+#", $argument)) {
        return false;
    } else {
        return true;
    }
}

/**
 * Permet de redimentionner une image
 * @param string $source chemin source
 * @param string $destination chemin de destination
 */
function resizeImage($source, $destination) {
    $height = 200;
    $extension = pathinfo($source, PATHINFO_EXTENSION);
    switch ($extension) {
        case "jpg":
        case "jpeg": //pour le cas où l'extension est "jpeg"
            $loadedImage = imagecreatefromjpeg($source);
            break;
        case "gif":
            $loadedImage = imagecreatefromgif($source);
            break;
        case "png":
            $loadedImage = imagecreatefrompng($source);
            break;
        default:
            $loadedImage = imagecreatefrompng($source);
            break;
    }
    $sizes = getimagesize($source);
    $ratio = $sizes[1] / $height;
    $widthRedim = $sizes[0] / $ratio;
    $imageDestination = imagecreatetruecolor($widthRedim, $height);
    imagesavealpha($imageDestination, true);
    imagefill($imageDestination, 0, 0, imagecolorallocatealpha($imageDestination, 0, 0, 0, 127));
    imagecopyresampled($imageDestination, $loadedImage, 0, 0, 0, 0, $widthRedim, $height, $sizes[0], $sizes[1]);
    $source = preg_replace("#(.jpg|.jpeg|.gif)$#", ".png", $source);
    imagepng($imageDestination, $destination);
    imagedestroy($loadedImage);
    imagedestroy($imageDestination);
}

/**
 * Permet de télécharger une image
 * @param string $url url de l'image
 * @param string $path Chemin où enregistrer l'image
 * @return int|false
 */
function download_image($url, $path, $id) {
    $extension = pathinfo($url, PATHINFO_EXTENSION);
    $image = file_get_contents($url);
    if ($extension == 'png') {
        return file_put_contents($path . $id . '.png', $image);
    } else {
        return imagepng(imagecreatefromstring($image), $path . $id . '.png');
    }
}
