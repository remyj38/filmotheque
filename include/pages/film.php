<?php

if (isset($_GET['argument']) && !empty($_GET['argument'])) {
    $id = preg_replace('#([\d]*)-.*#', '$1', $_GET['argument']);
    try {
        $film = Movie::get($id);
    } catch (Exception $ex) {
        $alerts[] = array('type' => 'warning', 'text' => $ex->getMessage());
        showView('list', Movie::getAll());
    }

    if ($film) {
        showView('film', $film);
    } else {
        $alerts[] = array('type' => 'warning', 'text' => 'Ce film n\'existe pas');
        showView('list', Movie::getAll());
    }
} else {
    $alerts[] = array('type' => 'warning', 'text' => 'Aucun film selectionné');
    showView('list', Movie::getAll());
}