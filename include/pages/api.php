<?php
ini_set('max_execution_time', 3000);
require 'include/classes/Api.class.php';
//header('Content-Type:application/json');
if (!isset($_GET['method'])) {
    $error = new ApiError('Missing arguments', 400);
    $error->throwFunction();
}
$api = new Api($_GET);
try {
    $api->exec();
} catch (ApiError $error) {
    $error->throwFunction();
}
echo $api->result(true);

