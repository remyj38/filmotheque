<h1><?= $datas->getTitle() ?></h1>
<ol class="breadcrumb">
    <li><a href="<?= BASE_URL ?>">Accueil</a></li>
    <li class="active"><?= $datas->getTitle() ?></li>
</ol>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
    <li class="active"><a href="#fiche" role="tab" data-toggle="tab">Fiche</a></li>
    <li><a href="#casting" role="tab" data-toggle="tab">Casting</a></li>
    <li><a href="#trailer" role="tab" data-toggle="tab">Trailer</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="fiche">
        <div class="row">
            <img src="<?php echo BASE_URL . 'medias/posters/' . $datas->getIdMovie(); ?>" title="Affiche du film" width="33%" style="float: left; margin: 20px">
            <div id="synopsis" class="text-justify" style="padding: 20px;">
                <?= $datas->getSynopsis(true); ?> 
            </div>
            <table class="table table-striped">
                <tr>
                    <td class="text-right">
                        Titre
                    </td>
                    <td>
                        <?= $datas->getTitle() ?> 
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        Titre original
                    </td>
                    <td>
                        <?= $datas->getOriginalTitle() ?> 
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        Année de production
                    </td>
                    <td>
                        <?= $datas->getProductionYear() ?> 
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        Durée
                    </td>
                    <td>
                        <?= (int) ($datas->getRuntime() / 60) ?> minutes
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        Genres
                    </td>
                    <td>
                        <?
                        $genres = $datas->getGenres();
                        foreach ($genres as $genre) {
                            echo $genre->getName();
                            if ($genre !== end($genres))
                                echo ', ';
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="tab-pane" id="casting">
        <?php
        /* @var $datas Movie */
        $roles = $datas->getRoles();
        if (isset($roles['8002'])) { // Réalisateur
            echo '<div class="row">';
            echo '<h2>Réalisateur' . ((count($roles['8002']) > 1) ? 's' : '') . ' <span class="badge">' . count($roles['8002']) . '</span></h2>';
            foreach ($roles['8002'] as $person) {
                ?>
                <div class="col-sm-6 col-md-2 text-center">
                    <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                        <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                            <div class="caption">
                                <h5><?= $person->getName() ?></h5>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
            echo '</div>';
            unset($roles['8002']);
        }
        if (isset($roles['8001'])) { // Acteurs
            echo '<div class="row">';
            echo '<h2>Acteur' . ((count($roles['8001']) > 1) ? 's' : '') . ' <span class="badge">' . count($roles['8001']) . '</span></h2>';
            /* @var $person Person */
            foreach ($roles['8001'] as $person) {
                ?>
                <div class="col-sm-6 col-md-2 text-center">
                    <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                        <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                            <div class="caption">
                                <h5><?= $person->getName() ?></h5>
                                <?php
                                if ($person->getRole())
                                    echo '<p>Dans le role de ' . $person->getRole() . '</p>';
                                ?>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
            echo '</div>';
            unset($roles['8001']);
        }

        if (isset($roles['8004'])) { // Scénario
            echo '<div class="row">';
            echo '<h2>Scénario <span class="badge">' . count($roles['8004']) . '</span></h2>';
            /* @var $person Person */
            foreach ($roles['8004'] as $person) {
                ?>
                <div class="col-sm-6 col-md-2 text-center">
                    <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                        <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                            <div class="caption">
                                <h5><?= $person->getName() ?></h5>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
            echo '</div>';
            unset($roles['8004']);
        }
        if (isset($roles['8029']) || isset($roles['8062']) || isset($roles['8064'])) { // Production
            echo '<div class="row">';
            echo '<h2>Production <span class="badge">' . (count($roles['8029']) + count($roles['8062']) + count($roles['8064'])) . '</span></h2>';
            /* @var $person Person */
            if (isset($roles['8029'])) {
                $role = Role::get(8029);
                foreach ($roles['8029'] as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            unset($roles['8029']);
            if (isset($roles['8062'])) {
                $role = Role::get(8062);
                foreach ($roles['8062'] as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            unset($roles['8062']);
            if (isset($roles['8064'])) {
                $role = Role::get(8064);
                foreach ($roles['8064'] as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            unset($roles['8064']);
            echo '</div>';
        }
        if (isset($roles['8003']) || isset($roles['8121'])) { // Musique
            echo '<div class="row">';
            echo '<h2>Musique <span class="badge">' . (count($roles['8003']) + count($roles['8121'])) . '</span></h2>';
            /* @var $person Person */
            if (isset($roles['8003'])) {
                $role = Role::get(8003);
                foreach ($roles['8003'] as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            unset($roles['8003']);
            if (isset($roles['8121'])) {
                $role = Role::get(8121);
                foreach ($roles['8121'] as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            unset($roles['8121']);
            echo '</div>';
        }

        if (count($roles)) { // Autres
            echo '<h2>Autres types de role <span class="badge">' . count($roles, COUNT_RECURSIVE) . '</span></h2>';
            /* @var $person Person */
            foreach ($roles as $id => $peoples) {
                $role = Role::get($id);
                foreach ($peoples as $person) {
                    ?>
                    <div class="col-sm-6 col-md-2 text-center">
                        <a href="<?= $person->getLink() ?>" target="blank" title="Voir la fiche sur allocine">
                            <div class="thumbnail"><img alt="poster" src="<?= $person->getPosterURL() ?>" />
                                <div class="caption">
                                    <h5><?= $person->getName() ?></h5>
                                    <p><?= $role->getName() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
        }
        ?>
    </div>
    <div class="tab-pane" id="trailer">
        <?php
        if ($datas->getIframeTrailer()) {
            echo '<div class="text-center">' . $datas->getIframeTrailer() . '</div>';
        }
        ?>
    </div>
</div>



<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
