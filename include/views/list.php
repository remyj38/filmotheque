<?php

/* @var $movie Movie */
foreach ($datas as $movie) {
    $genres = '';
    foreach ($movie->getGenres() as $genre)
        $genres .= ' ' . str_replace(' ', '-', $genre->getName());
    echo '<div class="col-sm-6 col-md-3 text-center filter' . $genres . '" style="height: 100px;">';
    echo '<a href="' . BASE_URL . 'film/' . $movie->getIdMovie() . '-' . $movie->getTitle() . '/" title="Voir la fiche de ' . $movie->getTitle() . '">';
    echo '<div class="thumbnail"><img alt="poster" src="' . BASE_URL . 'medias/posters/mini/' . $movie->getIdMovie() . '.png" />';
    echo '<p>' . $movie->getTitle() . '</p></div>';
    echo "</a></div>\n";
}