<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php $title; ?></title>
        <link href="<?php echo BASE_URL; ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo BASE_URL; ?>assets/js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/isotope.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <?php
            foreach ($alerts as $alert){
                echo '<p class="bg-' . $alert['type'] . '">' . $alert['text'] . '</p>';
            }
