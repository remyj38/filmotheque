$folder = Read-Host "Dossier � scanner " #Demande du dossier � scanner
$export_path = [Environment]::GetFolderPath("Desktop") + "\export.json"
$json = Get-ChildItem -File -recurse $folder | Select-Object Name | ConvertTo-JSON | Out-File -Encoding "UTF8" $export_path # Scan du dossier, Conversion en JSON et enregistrement du fichier
[System.Windows.Forms.MessageBox]::Show("Le fichier a �t� enregistr� sur votre bureau sous le nom de `"export.json`"", "Exportation r�ussie !")