<?php

require_once 'include/functions.php';
init();

if (isset($_GET['error'])) { // Si on demande une erreur, on l'affiche
    errors($_GET['error']);
}

if (isset($_GET['request'])) { // Si on veut afficher une page, on accède a son controleur seulement si la page est correcte
    if (!protectGet($_GET['request'])) {
        errors(404);
    }
    if (file_exists('include/pages/' . $_GET['request'] . '.php')) {
        include 'include/pages/' . $_GET['request'] . '.php';
    } else {
        errors(404);
    }
} else { // Sinon, on affiche les news
    include 'include/pages/list.php';
}